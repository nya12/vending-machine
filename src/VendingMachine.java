import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class VendingMachine {

    private JPanel root;
    private JLabel topLabel;
    private JButton MochacookieCrumbleFrappuccino;
    private JButton MochaFrappuccino;
    private JButton JavaChipFrappuccino;
    private JButton ChaiCremeFrappuccino;
    private JButton CoffeeFrappuccino;
    private JButton CaramelFrappuccino;
    private JButton checkOutButton;
    private JTextPane list;
    private JTextField total;
    double sum = 0;

    void order(String bevarage,String cresit,double num) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+bevarage+"?",
                "Order Confirmation", JOptionPane.YES_NO_OPTION);

        if(confirmation==0) {
            String currentText = list.getText();
            list.setText(currentText + bevarage +" " + cresit + "\n");
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + bevarage + ". It will be served as soon as possible.");
            list.getText();
            sum = sum +num;
            total.setText("Total $" +sum);

        }
    }


    public VendingMachine() {
        MochacookieCrumbleFrappuccino.addActionListener(new ActionListener(
        ) {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mocha cookie Crumble Frappuccino","$3.85",3.85);
            }
        });
        MochacookieCrumbleFrappuccino.setIcon(new ImageIcon(
                this.getClass().getResource("Mo.jpeg")
        ));

        JavaChipFrappuccino.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Java Chip Frappuccino","$3.95",3.95);
            }
        });
        JavaChipFrappuccino.setIcon(new ImageIcon(
                this.getClass().getResource("Ja.jpeg")
        ));

        CaramelFrappuccino.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                         order("Caramel Frappuccino","$4.35",4.35);
                           }
        });
        CaramelFrappuccino.setIcon(new ImageIcon(
                this.getClass().getResource("Cara.jpeg")
        ));

        MochaFrappuccino.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mocha Frappuccino","$4.50",4.50);
            }
        });
        MochaFrappuccino.setIcon(new ImageIcon(
                this.getClass().getResource("Mo.jpeg")
        ));

        CoffeeFrappuccino.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coffee Frappuccino","$4.70",4.70);
            }
        });
        CoffeeFrappuccino.setIcon(new ImageIcon(
                this.getClass().getResource("Coffee.jpeg")
        ));

        ChaiCremeFrappuccino.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chai Creme Frappuccino","$5.00",5.00);
            }
        });
        ChaiCremeFrappuccino.setIcon(new ImageIcon(
                this.getClass().getResource("Chai.jpeg")
        ));
        total.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "*Last Confirmation!*",
                        JOptionPane.YES_NO_OPTION);


                if (confirmation==0) {
                    JOptionPane.showMessageDialog(null,"Thank you! The total price is $"+ sum);
                    list.setText("");
                    total.setText("Total $0");
                    sum=0;
                }
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("VendingMachine");
        frame.setContentPane(new VendingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
